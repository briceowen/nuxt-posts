import Vue from 'vue'
import moment from 'moment'

Vue.filter('formatDate', (date, dateFromat = 'LL') => {
  if (!date) {
    return ''
  }

  return moment(date).format(dateFromat)
})
